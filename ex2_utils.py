import math
import numpy as np
import cv2 as cv
import cv2
import itertools


def myID() -> int:
    """
    Return my ID (not the friend's ID I copied from)
    :return: int
    """

    return 207964859


def conv1D(in_signal: np.ndarray, k_size: np.ndarray) -> np.ndarray:
    """
    Convolve a 1-D array with a given kernel
    :param in_signal: 1-D array
    :param k_size: 1-D array as a kernel
    :return: The convolved array
    """
    n, m = in_signal.shape[0], k_size.shape[0]
    k_size = np.flip(k_size)
    new_signal = np.ndarray((n + 2 * (m - 1))).astype(int)
    new_signal[:] = 0
    new_signal[m - 1:new_signal.shape[0] - m + 1] = in_signal
    ret = [new_signal[i:i+m] @ k_size for i in range(n + m - 1)]
    return np.asarray(ret)


def conv2D(in_image: np.ndarray, kernel: np.ndarray) -> np.ndarray:
    """
    Convolve a 2-D array with a given kernel
    :param in_image: 2D image
    :param kernel: A kernel
    :return: The convolved image
    """
    ny, nx = in_image.shape
    my, mx = kernel.shape

    new_image = np.ndarray((ny + 2 * (my - 1), nx + 2 * (mx - 1))).astype(int)
    new_image[:,:] = 0
    new_image[my - 1:new_image.shape[0] - my + 1, mx - 1:new_image.shape[1] - mx + 1] = in_image
    ret = np.ndarray((ny, nx))
    for j in range(ny):
        for i in range(nx):
            ret[j,i] = (new_image[j:j+my,i:i+mx] * kernel).sum()
    return ret


def convDerivative(in_image: np.ndarray) -> (np.ndarray, np.ndarray):
    """
    Calculate gradient of an image
    :param in_image: Grayscale iamge
    :return: (directions, magnitude)
    """
    A = np.asarray([1, 0, -1]).reshape((1, 3))
    in_image_x = conv2D(in_image, np.flip(A))
    in_image_y = conv2D(in_image, np.flip(A.T))

    magnitude = np.sqrt(in_image_x**2+in_image_y**2)
    directions = in_image_y.copy()
    directions[(in_image_x==0)&(in_image_y>0)] = np.pi/2
    directions[(in_image_x==0)&(in_image_y<0)] = -np.pi/2
    directions[(in_image_x==0)&(in_image_y==0)] = 0
    ids = (in_image_x!=0)
    directions[ids] = np.arctan(in_image_y[ids]/in_image_x[ids])
    return magnitude, directions


def blurImage1(in_image: np.ndarray, k_size: int) -> np.ndarray:
    """
    Blur an image using a Gaussian kernel
    :param in_image: Input image
    :param k_size: Kernel size
    :return: The Blurred image
    """
    kernal_1d = np.arange(k_size)
    kernal_1d = (kernal_1d-int(k_size/2))**2
    kernal_1d = np.exp(-kernal_1d/2)
    kernal_1d /= math.sqrt(2*np.pi)
    kx = np.tile(kernal_1d,(k_size, 1))
    kernal = kx * kx.T
    kernal /= kernal.sum()
    return conv2D(in_image, kernal)


def blurImage2(in_image: np.ndarray, k_size: int) -> np.ndarray:
    """
    Blur an image using a Gaussian kernel using OpenCV built-in functions
    :param in_image: Input image
    :param k_size: Kernel size
    :return: The Blurred image
    """
    kernal_1d = cv.getGaussianKernel(k_size, -1)
    kernal = kernal_1d * kernal_1d.T
    kernal /= kernal.sum()
    return conv2D(in_image, kernal)


def edgeDetectionZeroCrossingSimple(img: np.ndarray) -> np.ndarray:
    """
    Detecting edges using "ZeroCrossing" method
    :param img: Input image
    :return: Edge matrix
    """
    kernel_1d = cv.getGaussianKernel(65, -1)
    kernel = kernel_1d * kernel_1d.T
    kernel /= kernel.sum()

    return cv.filter2D(src=img, ddepth=-1, kernel=kernel) - img


def edgeDetectionZeroCrossingLOG(img: np.ndarray) -> np.ndarray:
    """
    Detecting edges using "ZeroCrossingLOG" method
    :param img: Input image
    :return: Edge matrix
    """

    blur = cv.GaussianBlur(img, (5, 5), 0)
    laplacian = cv.Laplacian(blur, cv.CV_64F)
    laplacian /= laplacian.max()
    return laplacian


def houghCircle(img: np.ndarray, min_radius: int, max_radius: int, circle_point_size: int = 360, trachold: float = 0.5) -> list[tuple[float, float, float]]:
    """
    Find Circles in an image using a Hough Transform algorithm extension
    To find Edges you can Use OpenCV function: cv2.Canny
    :param img: Input image
    :param min_radius: Minimum circle radius
    :param max_radius: Maximum circle radius
    :return: A list containing the detected circles,
                [(x,y,radius),(x,y,radius),...]
    """
    h, w = img.shape

    img = (img * 255).astype(np.uint8)
    edges_img = cv2.Canny(img, 100, 200)
    edges_img[edges_img < 255] = 0
    edges_img[edges_img >= 255] = 255

    radiuss_count = max_radius - min_radius + 1

    # create circles grid
    circles_grid = np.zeros((radiuss_count, circle_point_size, 2))  # (r, angle, x/y)
    for r in range(min_radius, max_radius + 1):
        for angle_index in range(circle_point_size):
            angle = 2 * np.pi * angle_index / circle_point_size
            circles_grid[r - min_radius, angle_index, 0] = int(r * np.sin(angle))  # cond y
            circles_grid[r - min_radius, angle_index, 1] = int(r * np.cos(angle))  # cond x

    circles_grid = circles_grid.astype(int)

    edges = list(zip(*((edges_img == 255).nonzero())))

    counter = np.zeros((img.shape[0], img.shape[1], radiuss_count))

    # hough algo
    for y, x in edges:
        ys = y + circles_grid[:, :, 0]
        xs = x + circles_grid[:, :, 1]

        ids = ((xs >= 0) & (w > xs) & (ys >= 0) & (h > ys))

        rs, ais = ids.nonzero()[0], ids.nonzero()[1]

        counter[ys[rs, ais], xs[rs, ais], rs] += 1

    res_raw = list(zip(*((counter > trachold * circle_point_size).nonzero())))

    # convert to circles
    res = [(v[1], v[0], v[2] + min_radius) for v in res_raw]

    # find circles with maxed radius's
    circles = [(key[0], key[1], max([item[2] for item in group])) for key, group in itertools.groupby(res, lambda x: (x[0], x[1]))]

    return circles


def bilateral_filter_implement(in_image: np.ndarray, k_size: int, sigma_color: float, sigma_space: float) -> (
        np.ndarray, np.ndarray):
    """
    :param in_image: input image
    :param k_size: Kernel size
    :param sigma_color: represents the filter sigma in the color space.
    :param sigma_space: represents the filter sigma in the coordinate.
    :return: OpenCV implementation, my implementation
    """

    dest_cv2 = cv.bilateralFilter(in_image, k_size, sigma_color, sigma_space)

    dest = np.zeros(in_image.shape)
    h, w = in_image.shape
    signal = np.zeros((in_image.shape[0] + 2 * k_size, in_image.shape[1] + 2 * k_size))
    signal[k_size: k_size + h, k_size: k_size + w] = in_image

    gassing_kernel_1d = cv.getGaussianKernel(2 * k_size + 1, sigma_space)
    gassing_kernel = gassing_kernel_1d @ gassing_kernel_1d.T

    for y in range(h):
        for x in range(w):
            sy, sx = y + k_size, x + k_size
            neighbors = signal[sy - k_size: sy + k_size + 1, sx - k_size: sx + k_size + 1]

            gasussian_color_diff = np.exp(-np.power(in_image[y,x] - neighbors, 2) / (2 * sigma_color))

            v = gassing_kernel * gasussian_color_diff * neighbors

            dest[y, x] = (v * neighbors).sum() / v.sum()

    return dest_cv2, dest
